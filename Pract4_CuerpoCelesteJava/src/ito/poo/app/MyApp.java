package ito.poo.app;

import java.time.LocalDate;

import Pract4_CuerpoCelesteUML.CuerpoCeleste;
import Pract4_CuerpoCelesteUML.Ubicacion;

public class MyApp {

	public static void main(String[] args) {
		
		CuerpoCeleste c=new CuerpoCeleste("Estrella", "solido");
	    System.out.println(c);
	    
	    Ubicacion c1=new Ubicacion(94.05f, 239.47f, "3 meses", 940.10f);
	    c.agregaUbicacion(c1);
	    
	    System.out.println(c); 
	    
	    c.agregaUbicacion(new Ubicacion(98.32f, 820.02f, "2 meses", 1930.20f));
	    c.agregaUbicacion(new Ubicacion(100.1f, 1839.2f, "1 meses", 1203.23f));
	    System.out.println(c); 

	    System.out.println(c.Desplazamiento(0, 2)); 

	}
}
